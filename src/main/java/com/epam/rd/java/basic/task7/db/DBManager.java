package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static final String URL = "jdbc:derby:memory:testdb;create=true";

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {

    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM users");) {
            while (rs.next()) {
                User p = new User();
                p.setId(rs.getInt("id"));
                p.setLogin(rs.getString("login"));
                users.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();) {
            String sql = "INSERT INTO users VALUES (DEFAULT, '" + user.getLogin() + "')";
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();) {
            for (User user : users) {
                String sql = "DELETE FROM users WHERE id = '" + user.getId() + "'";
                stmt.executeUpdate(sql);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM users WHERE login = '" + login + "'");) {
            user.setId(rs.getInt("id"));
            user.setLogin(rs.getString("login"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM teams WHERE name = '" + name + "'");) {
            team.setId(rs.getInt("id"));
            team.setName(rs.getString("name"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM teams p ORDER BY p.name");) {
            while (rs.next()) {
                Team p = new Team();
                p.setId(rs.getInt("id"));
                p.setName(rs.getString("name"));
                teams.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();) {
            String sql = "INSERT INTO teams VALUES (DEFAULT, '" + team.getName() + "')";
            stmt.executeUpdate(sql);
            ResultSet rs = stmt.executeQuery("SELECT * FROM teams WHERE name = '" + team.getName() + "'");
                if (rs.next()) team.setId(rs.getInt("id"));
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        String sql = "INSERT INTO users_teams VALUES ";
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();) {
            for (Team team : teams) {
                sql += " ((SELECT id FROM users WHERE login = '" + user.getLogin() + "'),(SELECT id FROM teams WHERE name = '" + team.getName()+ "')),";
            }
            sql = sql.substring(0,sql.length()-1);
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException(e.getMessage(), e.getCause());
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM teams WHERE id IN (SELECT team_id FROM users_teams WHERE user_id = (SELECT id FROM users WHERE login = '" + user.getLogin() + "'))");) {
            while (rs.next()) {
                Team p = new Team();
                p.setId(rs.getInt("id"));
                p.setName(rs.getString("name"));
                teams.add(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();) {
                String sql = "DELETE FROM teams WHERE name = '" + team.getName() + "'";
                stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection con = DriverManager.getConnection(URL);
             Statement stmt = con.createStatement();) {
            String sql = "UPDATE teams SET name = '" + team.getName() + "' WHERE id = " + team.getId();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
